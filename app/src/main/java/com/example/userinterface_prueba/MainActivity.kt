package com.example.userinterface_prueba

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(),formFragment.OnFragmentInteractionListener,Succes_screenFragment.OnFragmentInteractionListener {
    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onFragmentInteraction(string: String) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, Succes_screenFragment.call())
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeComponents()
    }

    private fun initializeComponents(){
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, formFragment.call())
            .commit()
    }


}
